package sbu.cs.parser.html;

import java.util.ArrayList;

public class Tag {
    private String tag;
    private String inside;
    private ArrayList<Key> keys;

    public Tag(){
        keys = new ArrayList<Key>();
    }

    public Tag(String tag , String inside , Key key){
        this.tag = tag;
        this.inside = inside;
        keys = new ArrayList<Key>();
        keys.add(key);
    }

    public void parseKey(String doc){
        boolean is_value = false;
        boolean has_key = true;
        int beg = 0;
        int end;
        int i = 0;
        ArrayList<String> listkeys = new ArrayList<String>();
        ArrayList<String> listvalues = new ArrayList<String>();
        while(doc.charAt(i) != ' '){
            i++;
            if(doc.charAt(i) == '>'){
                has_key = false;
                break;
            }
        }
        tag = doc.substring(0,i);
        doc = doc.substring(i,doc.length());
        doc = doc.replaceAll(" +"," ");
        for (i=0 ; i < doc.length() && has_key ; i++){
            if(doc.charAt(i) == '"'){
                if (is_value){
                    end = i+1;
                    listvalues.add(doc.substring(beg+1 , end-1).trim());
                    is_value = false;
                    beg = i + 1;
                }
                else if(!is_value) {
                    beg = i;
                    is_value = true;
                }
            }
            if(doc.charAt(i) == '='){
                end = i;
                listkeys.add(doc.substring(beg , end).trim());
            }
        }
        keys.add(new Key(listkeys , listvalues));
    }

    public String getTag() {
        return tag;
    }

    public void addTag(String tag){
        this.tag = tag;
    }

    public void addInside(String inside){
        this.inside = inside;
    }

    public void addKey(ArrayList<String> key , ArrayList<String> value){
        keys.add(new Key(key,value));
    }

    /*public int getSize(){return tags.size();}*/

    public String getValue(String key){return keys.get(0).getValue(key);}

    public Key getKey(int index){return keys.get(index);}

    public String getInsides() {
        return inside;
    }
}
