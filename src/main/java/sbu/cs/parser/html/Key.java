package sbu.cs.parser.html;

import java.util.ArrayList;

public class Key {
    private ArrayList<String> keys;
    private ArrayList<String> values;

    public Key(){
        keys = new ArrayList<String>();
        values = new ArrayList<String>();
    }

    public Key(ArrayList<String> keys , ArrayList<String> values) {
        this.keys = keys;
        this.values = values;
    }

    public String getValue(String key){
        for (int i = 0 ; i < keys.size() ; i++){
            if(keys.get(i).equals(key)){
                return values.get(i);
            }
        }
        return null;
    }
}
