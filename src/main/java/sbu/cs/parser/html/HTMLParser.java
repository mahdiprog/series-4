package sbu.cs.parser.html;

import java.util.ArrayList;

public class HTMLParser {

    /*
    * this function will get a String and returns a Dom object
     */
    public static Node parse(String document) {
        Tag tag = new Tag();
        ArrayList<Node> child = new ArrayList<>();
        document = document.replaceAll("\n","");
        int i , beg , end , n;
        beg = end  = 0;
        boolean has_key = false;
        for (i = 0 , n = 0 ; i < document.length() ; i++) {
            if (document.charAt(i) == '<' && document.charAt(i + 1) != '/') {
                beg = i;
                while (document.charAt(i) != '>') {
                    i++;
                }
                tag.addTag(document.substring(beg, i));
                String end_tag;
                end_tag = "</" + tag.getTag();
                end = i + 1;
                i = document.length() - end_tag.length();
                tag.addInside(document.substring(end , i));
                child = HTMLParser.getChild(tag.getInsides());
            }
        }
        Node node = new Node(tag , child);
        return node;
    }

    public static ArrayList<Node> getChild(String document){
        if(document == null){
            return null;
        }
        ArrayList<Tag> tags = new ArrayList<>();
        ArrayList<Node> nodes = new ArrayList<>();
        document = document.replaceAll("\n","");
        int i , beg , end , n;
        beg = end  = n = 0;
        boolean has_key = false;
        for (i = 0 , n = 0 ; i < document.length() ; i++){
            if(document.charAt(i) == '<' && document.charAt(i+1) != '/'){
                tags.add(new Tag());
                int j = i;
                beg = i + 1;
                while(document.charAt(j) != '>'){
                    if(document.charAt(j) == ' ' && document.charAt(j+1) != '>'){
                        int k;
                        for (k = j+1 ; document.charAt(k) != '>' ; k++){}
                        tags.get(n).parseKey(document.substring(beg , k));
                        j = k+1;
                        has_key=true;
                        break;
                    }
                    j++;
                }
                if(!has_key) {
                    tags.get(n).addTag(document.substring(beg, j));
                }
                String end_tag = new String();
                end_tag = "</"+tags.get(n).getTag();
                beg = j + 1;
                while(!(document.substring(j,j + end_tag.length()).equals(end_tag))){
                    j++;
                }
                end = j;
                tags.get(n).addInside(document.substring(beg, end));
                i = j;
                if(!has_key){
                    tags.get(n).addKey(null , null);
                }
                n++;
                has_key=false;
            }
            else if (document.charAt(i) == '<' && document.charAt(i+1) == '/'){
                int j = i+1;
                while(document.charAt(j) != '>'){
                    j++;
                }
                tags.add(new Tag());
                tags.get(n).parseKey(document.substring(i+2 , j + 1));
                tags.get(n).addInside(null);
                n++;
                has_key=false;
                i = j;
            }
        }
        for (Tag tag:tags) {
            nodes.add(new Node(tag , HTMLParser.getChild(tag.getInsides())));
        }
        return nodes;
    }

    /*
    * a function that will return string representation of dom object.
    * only implement this after all other functions been implemented because this
    * impl is not required for this series of exercises. this is for more score
     */
    public static String toHTMLString(Node root) {
        // TODO implement this for more score
        return null;
    }
}
