package sbu.cs.parser.json;

import java.util.ArrayList;

public class Json implements JsonInterface {

    private ArrayList<Key> keys;

    public Json(ArrayList<Key> keys){
        this.keys = keys;
    }
    @Override
    public String getStringValue(String key){
        for (int i = 0 ; i < keys.size() ; i++){
            if(keys.get(i).getKey().equals(key)){
                return keys.get(i).getValue();
            }
        }
        return null;
    }
}
