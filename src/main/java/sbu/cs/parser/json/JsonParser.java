package sbu.cs.parser.json;

import java.util.ArrayList;

public class JsonParser {

    /*
    * this function will get a String and returns a Json object
     */
    public static Json parse(String data) {
        ArrayList<Key> keys = new ArrayList<Key>();
        //ArrayList values = new ArrayList<String>();
        String key = "";
        String value = "";
        boolean is_key = false;
        //boolean value = false;
        boolean exist_word = false;
        int beg = 0;
        int end = 0;
        int i;
        for (i = 0 ; i < data.length() ; i++){
            beg = i;
            if(data.charAt(i) == '['){
                int arr_beg = i;
                while(data.charAt(i) != ']'){
                    i++;
                }
                int arr_end = ++i;
                exist_word = true;
            }
            while(Character.isAlphabetic(data.charAt(i)) || Character.isDigit(data.charAt(i))){
                i++;
                exist_word = true;
            }
            end = i;
            if(!is_key && exist_word){
                key = data.substring(beg , end);
                is_key = true;
                exist_word = false;
            }
            else if (exist_word){
                value = data.substring(beg , end);
                is_key = false;
                exist_word = false;
                keys.add(new Key(key , value));
            }
        }
        /*for (i = 0 ; i < values.size() ; i++){
            System.out.println(values.get(i));
        }*/
        Json result = new Json(keys);
        return result;
    }

    /*
    * this function is the opposite of above function. implementing this has no score and
    * is only for those who want to practice more.
     */
    public static String toJsonString(Json data) {
        return null;
    }
}
