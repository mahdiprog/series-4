package sbu.cs.parser.json;

import java.util.ArrayList;

public class Key {
    private String key;
    private String value;

    public Key(String key ,String value){
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }
}
